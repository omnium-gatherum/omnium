# OMNIUM #



### What is OMNIUM? ###

**OMNIUM** (**O**verall **M**eetings & **N**etworking of **I**ndividuals **U**nited for the Advancement of Hu**M**anity) 
is a global problem-solving matrix implemented through crowdsourcing and crowdfunding.

**OMNIUM Gatherum** is the virtual spatialverse where avatars can congregate.